﻿using System;
using System.Collections.Generic;

namespace Delegates
{
    internal class Program
    {
        const int numberProcessedFiles = 15;
        static void Main(string[] args)
        {
            string dir = @"C:\Users\Public";

            Console.WriteLine("Scan dir: " + dir);

            ScannerSystem scanner = new ScannerSystem();

            scanner.RaiseFileFoundEvent += FileReceiver;

            scanner.Scan(dir);

            Console.WriteLine("Finish.");

            List<FinderClass> list = new List<FinderClass> 
            {
                new FinderClass(100.1f),
                new FinderClass(450.1f),
                new FinderClass(-24.1f)
            };

            Console.Write("\nПоиск максимального элемента из: ");

            foreach (FinderClass item in list)
            {
                Console.Write(item.F + "; ");
            }

            var max = list.GetMax<FinderClass>(e => e.F);
            
            Console.WriteLine("\nMax value is: " + max.F);
        }

        private static void FileReceiver(object sender, FileArgs file)
        {
            Console.WriteLine("Event up. File:" + file.FilePath);

            ScannerSystem scanner = (ScannerSystem)sender;

            if (scanner.Counter > numberProcessedFiles)
            {
                // Прерывание обработки
                scanner.StopScanning();
            }
        }
    }
}
