﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    public class FinderClass
    {
        public float F { get; set; }
        public FinderClass(float f)
        {
            F = f;
        }
    }
}
