﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    public class ScannerSystem
    {
        public event EventHandler<FileArgs> RaiseFileFoundEvent;

        private bool _bProcess;
        public int Counter { get; set; }

        public void Scan(string path)
        {
            _bProcess = true;

            Counter = 0;

            if (Directory.Exists(path))
            {
                ProcessDirectory(path);
            }
            else if (File.Exists(path))
            {
                ProcessFile(path);
            }
            else
            {
                Console.WriteLine("Path " + path + " doesn't exist.");
            }
        }

        private void ProcessDirectory(string targetDirectory)
        {
            if (!_bProcess)
            {
                return;
            }
                

            var fileEntries = Directory.GetFiles(targetDirectory);

            foreach (string fileName in fileEntries)
            {
                if (!_bProcess)
                {
                    return;
                }

                ProcessFile(fileName);
            }

            var subdirectoryEntries = Directory.GetDirectories(targetDirectory);

            foreach (string subdirectory in subdirectoryEntries)
            {
                if (!_bProcess)
                {
                    return;
                }

                ProcessDirectory(subdirectory);
            }
        }

        private void ProcessFile(string path)
        {
            Counter++;

            RaiseFileFoundEvent?.Invoke(this, new FileArgs(path));
        }

        public void StopScanning()
        {
            Console.WriteLine("Прерывание обработки");

            _bProcess = false;
        }
    }
}
